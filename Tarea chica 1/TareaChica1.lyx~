#LyX 2.1 created this file. For more info see http://www.lyx.org/
\lyxformat 474
\begin_document
\begin_header
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_math auto
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Tarea Chica IIC1005
\end_layout

\begin_layout Author
Franco Bruña (fgbruna@uc.cl)
\end_layout

\begin_layout Date
17 de Abril del 2017
\end_layout

\begin_layout Section
Tutorial Codecademy
\end_layout

\begin_layout Standard
Screenshot completación tutorial Codecademy.
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename completed.png
	width 60page%
	height 60pheight%
	keepAspectRatio

\end_inset


\end_layout

\begin_layout Section
Manipulación de archivos
\end_layout

\begin_layout Standard
Secciones y subsecciones se describen más adelante.
\end_layout

\begin_layout Subsection
PrimerosPasos.md
\end_layout

\begin_layout Itemize

\family typewriter
pwd 
\family default
para mostrar el directorio en que nos encontramos
\end_layout

\begin_layout Itemize

\family typewriter
ls y ls *.txt 
\family default
para mostrar los directorios/archivos presentes y los archivos terminados
 en .txt respectivamente.
\end_layout

\begin_layout Itemize
Por último 
\family typewriter
cat *.txt 
\family default
para mostrar el contenido de todos los archivos terminados en *.txt del directori
o.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename primeroPasosmd.png
	width 60page%
	height 60pheight%
	keepAspectRatio

\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename tareachica1.png
	width 60page%
	height 60pheight%
	keepAspectRatio

\end_inset


\end_layout

\begin_layout Subsection
NombreApellido y mover archivos tareaChica*.bla
\end_layout

\begin_layout Itemize
Se abre otra terminal y se utiliza 
\family typewriter
cd 
\family default
para navegar hasta TareaChica1
\end_layout

\begin_layout Itemize
Utilizando el comando 
\family typewriter
mkdir FrancoBruña
\family default
 creamos un directorio vacío con nombre FrancoBruña.
\end_layout

\begin_layout Itemize
Luego utilizamos el comando 
\family typewriter
find .
 -name 'tareaChica*.bla' 
\family default
para encontrar todos los archivos que se enncuentran tanto en el directorio
 actual ( .
 ) como en los hijos de él y que cumplen que su nombre empieza con el string
 tareaChica y su extensión es .bla.
 Luego con 
\family typewriter
-exec mv {} FrancoBruña/ 
\backslash
; 
\family default
le pediremos a bash que ejecute el comando move hacia el directorio FrancoBruña/
 por cada elemento que find le entregue.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename tareachica3.png
	width 60page%
	height 60pheight%
	keepAspectRatio

\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename tareachicamover.png
	width 60page%
	height 60pheight%
	keepAspectRatio

\end_inset


\end_layout

\begin_layout Subsection
Obtención bytes totales NombreApellido
\end_layout

\begin_layout Itemize
Utilizamos
\family typewriter
 cat *.bla >> bytesCount.txt
\family default
 para enviar todo el contenido de los archivos con terminacion .bla a un
 nuevo archivo llamado bytesCount.txt.
\end_layout

\begin_layout Itemize
Luego utilizamos 
\family typewriter
ls
\family default
 para listar todos los archivos del directorio FrancoBruña.Agregamos el parametro
 
\family typewriter
-l
\family default
 para establecer el formato de salida largo.
\end_layout

\begin_layout Itemize
La informacion de la cuarta columna para el archivo bytesCount.txt nos entrega
 la cantidad de bytes totales de todos los archivos .bla.
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename tareachicabytes.png
	width 60page%
	height 60pheight%
	keepAspectRatio

\end_inset


\end_layout

\begin_layout Subsection
Bonus git.
\end_layout

\begin_layout Itemize
Primero definimos una función en nano y la guardamos en un archivo bash_profile.
\end_layout

\begin_layout Itemize
La función definida como 
\family typewriter
gitfunc() 
\family default
recibe un string entre comillas como mensaje,ejecuta el comando
\family typewriter
 git add -A
\family default
, le entrega el string al comando 
\family typewriter
git commit -m a traves de 
\begin_inset Quotes eld
\end_inset

$@
\begin_inset Quotes erd
\end_inset

 
\family default
y luego ejecuta el comando 
\family typewriter
git push
\family default
 en el repositorio en donde estamos ubicados.
\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename bonusgit.png
	width 60page%
	height 60pheight%
	keepAspectRatio

\end_inset


\end_layout

\begin_layout Standard
\align center
\begin_inset Graphics
	filename bonusgit3.png
	width 60page%
	height 60pheight%
	keepAspectRatio

\end_inset


\end_layout

\end_body
\end_document
