%Rama
rama(nil).
rama([R],arbol(X,nil,nil)):-R=X.
rama([R|T],arbol(R,I,D)):-rama(T,I);rama(T,D).
%Prerama
prerama(nil).
prerama([H|[]],arbol(X,_,_)):-H=X.
prerama([R|T],arbol(R,I,D)):-prerama(T,I);prerama(T,D).
%arbol(a, arbol(b, arbol(d,nil,nil), arbol(e, arbol(g,nil,nil), nil)), arbol(c, nil, arbol(f,nil,nil)))
