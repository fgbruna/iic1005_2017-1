%Utilidades
%Comparación nodos.
mayor(X,arbol(Y,_,_)):-X>Y.
menor(X,arbol(Y,_,_)):-X=<Y.
%Principal.
abb(nil).
abb(arbol(_,nil,nil)).
abb(arbol(X,nil,D)):-menor(X,D),abb(D).
abb(arbol(X,I,nil)):-mayor(X,I),abb(I).
abb(arbol(X,I,D)):-mayor(X,I),menor(X,D),abb(I),abb(D).
%https://github.com/agiuliano/prolog-scripts/blob/master/bst.pl
