:- use_module(library(clpfd)).

bonus(X) :-
    length(_, Nodes),
    bonus(X, Nodes).

bonus(nil, 0).
bonus(arbol(_,I,D), N) :-
    N > 0,
    Ni + Nd #= N - 1,
    Ni #>= 0, Nd #>= 0,
    label([Ni,Nd]),
    bonus(I, Ni),
    bonus(D, Nd).
    %Pregunta hecha por mí en stackoverflow
    %https://stackoverflow.com/questions/44400495/get-all-possible-binary-trees-using-prolog
