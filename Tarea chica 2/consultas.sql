--Consultas Tarea chica 2 IIC1005
--1
SELECT * FROM movies
WHERE release_date BETWEEN "1996-02-01 00:00:00" AND  "1996-02-29 23:59:59";
--2
SELECT COUNT(users.occupation_id) AS 'suma',occupations.name FROM users--Selecciono las repeticiones de cada id y las ocupaciones.
INNER JOIN occupations ON users.occupation_id = occupations.id --Cruzo occupations con la condición dada.
GROUP BY occupations.name--Agrupo los resultados de 'suma' por nombre de ocupación.
ORDER BY suma DESC--Ordeno respecto a 'suma' de manera descendente.
LIMIT 5;--Entrego los primeros 5 resultados.
--3
SELECT ROUND(AVG(promedio),2) as "prom",genero
FROM(
  SELECT ratings.rating as "promedio" ,genres.name AS "genero" FROM ratings
  JOIN genres_movies ON genres_movies.movie_id = ratings.movie_id
  JOIN genres ON genres_movies.genre_id = genres.id
  GROUP BY ratings.movie_id
)
GROUP BY genero;
--4
SELECT pelicula,COUNT(pelicula) AS "rates"
FROM
(SELECT movies.title AS "pelicula",ratings.rating AS "evaluacion"
FROM ratings
JOIN movies ON movies.id = ratings.movie_id--cruzo id's de movies con id's de ratings
JOIN users ON users.id = ratings.user_id--cruzo los id's de los usuarios en ratings y users
WHERE users.age < 35)--menores de 35 años
GROUP BY pelicula
ORDER BY rates DESC
LIMIT 10;
--Bonus
/*Con una sola consulta, obtener la edad promedio de las usuarios mujeres cuyo rating
promedio es mayor a 2.5.*/
SELECT ROUND(AVG(users.age),2) AS "prom" FROM users
JOIN ratings ON users.id = ratings.user_id
WHERE users.gender = "F"
GROUP BY users.gender
HAVING prom > 2.5;
