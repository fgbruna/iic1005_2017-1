# Readme Tarea Grande 2

## Por favor, considerar para la corrección:

* Para poder tener la visualización, será necesario:

    1. Incluir el archivo `dataset.txt` en la carpeta principal .Haber ejecutado cada input
         de `Tarea2.ipynb` para la creación del archivo `data.csv`.

    2. Guardar las imágenes en la carpeta `img` y las miniaturas  en la subcarpeta
         `thumb`.

* Algunos inputs de `Tarea2.ipynb` tardan _un poco más_ de 1 minuto en ejecutarse.
