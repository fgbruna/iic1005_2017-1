d3.csv("tweet_pos_sample.csv", function(data) {

    // Inicializar mapa

    var map = L.map('miMapa').setView([-33.49815589626299, -70.6153847676942], 10);
    var pinsLayer  = L.layerGroup();
    var base_layer = L.tileLayer('https://api.mapbox.com/styles/v1/fgbruna/cj0ubxftn00g42rpnjr2om5of/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZmdicnVuYSIsImEiOiJjajB1YThyaTYwNGMxMzJtdDZ0ZG10YWZmIn0.Dz2suwogOPd5cxOV4PkTkQ', {

        maxZoom: 20,

        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'

    });
    var myIcon = L.icon({
        iconUrl: 'Twitter-icon.png',
        iconSize: [20, 20],
    });
    base_layer.addTo(map);
    data.forEach(function(d) {
        var newMarker = L.marker([d.lat, d.lon], {
            icon: myIcon
        });
        newMarker.bindPopup(d.screen_name + " | " + d.creation_date);
        newMarker.addTo(map);
        pinsLayer.addLayer(newMarker);
    });
});
//defino los charts
var tiempoChart = dc.lineChart("#tweets-tiempo");
var tweetChart = dc.rowChart("#tabla-ciudades");
var dataTable = dc.dataTable("#dc-table-graph");

d3.csv("tweet_pos_sample.csv", function(data) {
    var cf = crossfilter(data);
    //rowChart
    var ciudadesDimension = cf.dimension(function(d) {
        return d.ubication;
    });
    var tweetsCiudadDimension = ciudadesDimension.group().reduceCount();
    //linechart
    var dateFormat = d3.time.format("%Y-%m-%d");
    var dateFormat2 = d3.time.format("%d")
    data.forEach(function(d) {
        if (d.creation_date.substring(8, 10).charAt(0) === 0) {
            d.dia = +d.creation_date.charAt(9)
        } else {
            d.dia = +d.creation_date.substring(8, 10);
        }

        //d.creation_date = dateFormat(d.creation_date)
    });

    var tiempoDimension = cf.dimension(function(d) {
        //console.log(d3.time.day(d.dia));
        return d.dia;
    });

    var tweetsGroup = tiempoDimension.group();

    tiempoChart
        .width(500)
        .height(500)
        .elasticY(true)
        .x(d3.scale.linear().domain([1, 31]))
        .renderDataPoints(true)
        .renderArea(true)
        .dimension(tiempoDimension)
        .group(tweetsGroup)
    //rowchart
    tweetChart
        .width(500)
        .height(500)
        .elasticX(true)
        .dimension(ciudadesDimension)
        .group(tweetsCiudadDimension)
        .x(d3.scale.linear().domain([0, 20]))
        .ordering(function(d) {
            return -d.value;
        });
   //tabla
   var dateformat = d3.time.format("%Y-%m-%d");
   data.forEach(function(d) {
     d.dtg = dateformat.parse(d.creation_date.substr(0, 11));
     d.lat = +d.lat;
     d.long = +d.lon;
   });
            // Create dataTable dimension
    var timeDimension = cf.dimension(function(d) {
      return d.dtg;
    });
            // Setup the charts
    dataTable.width(600).height(250)
    .dimension(timeDimension)
    .group(function(d) {
      return "Tabla de datos"
    })
    .size(10)
    .columns([function(d) {
      return d.screen_name;
    }, function(d) {
      return d.text_tweet;
    }, function(d) {
      return d.dtg;
    }])
    .sortBy(function(d) {
      return d.dtg;
    })
    .order(d3.ascending);


    dc.renderAll();
});
