## Abrir .svg con Mozilla Firefox en caso de que el pdf no se vea bien.

Supuestos
---------------
* Despacho debe enviar una notificación a contabilidad una vez que hayan vuelto a la central
* En el ajuste del pedido el cliente puede cambiar su pedido por otro que si tenga stock,por lo que no se vuelve a a la revisión de stock.
* El solo se inicia una vez entregada la guía por parte de contabilidad.
