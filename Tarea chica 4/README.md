Explicación máquina
================
Conceptualmente
==============
* Notar que si queremos comprobar que un *heap* es un *binary heap*
   podemos aprovechar una propiedad que se cumple para todo input,**que sea binary heap entregado con el formato propuesto.**
   Si tomamos en cuenta ambos ejemplos del enunciado:

   * Caso 1
  >011.100.110.111.101

   >101.111.110.100.011

   * Caso 2
   >0001.0010.0100.0110.0011.0111.1001.1000.1100

    >1100.1000.1001.0111.0011.0110.0100.0010.0001

  Podemos notar que en ambos se cumple que,de izquierda a derecha,*hasta cierto nodo*, todos los numero binarios del heap superior serán menores que el del inferior.Pero luego de ese cierto nodo,en que el valor sera común,la relación se invierte,y notamos que los valores de arriba serán mayores que los de abajo.

Técnicamente
===========
* Como pudimos ver en la sección anterior lo único que deberá hacer
   nuestra máquina será:
   1. Copiar el string entregado de forma invertida bajo el input.
   2. Recorrer de derecha a izquierda ambas cintas.
   3. Primero debe chequear que los valores superiores sean de mayor magnitud,hasta llegar a un punto en que serán iguales.
   4. Después de ese punto debe chequear que todos los valores superiores sean menores.

#### Disclaimer #
   El método aplicado falla en el caso específico en que uno de los nodos "rompa" la propiedad  de binary heap,*i.e* lo valida ya que se cumple la propiedad propuesta antes, pero el input no es *ni min ni max heap.*
